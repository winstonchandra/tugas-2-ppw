from django.conf.urls import url
from .views import index, editProfile, integrateData, addKeahlian

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit-profile/$', editProfile, name='edit-profile'),
    url(r'^add-keahlian/$', addKeahlian, name='add-keahlian'),

    url(r'^integrate/$', integrateData, name='integrate')
    ]
from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from .models import Pengguna,Keahlian

# Create your views here.
response = {}
def index(request):
	if 'user_login' in request.session:
		html = 'app_profile/profile.html'
		response['npm'] = request.session['kode_identitas']
		getDataFromModel(request)
		return render(request, html,response)
	else:
		HttpResponseRedirect(reverse('login'))

def editProfile(request):
	if 'user_login' in request.session:
		html = 'app_profile/edit_profile.html'
		response['npm'] = request.session['kode_identitas']
		getDataFromModel(request)
		return render(request, html,response)
	else:
		HttpResponseRedirect(reverse('login'))
def integrateData(request):
	if(request.method == 'POST'):
		nama = request.POST.get('first-name') +' '+request.POST.get('last-name')
		npm = request.session['kode_identitas']
		email = request.POST.get('email')
		profile = request.POST.get('link')
		pengguna = Pengguna(nama = nama, npm = npm, email = email, profile = profile)
		HttpResponseRedirect(reverse('profile:edit-profile'))

def addKeahlian(request):
	if(request.method == 'POST' and isIntegrated()):
		pengguna = Pengguna.objects.filter(npm = request.session['kode_identitas'])[0]
		keahlian = Keahlian(jenis = request.POST.get('keahlian'), level = request.POST.get('level'), pengguna = pengguna)
		keahlian.save()
		HttpResponseRedirect(reverse('profile:edit-profile'))

def getDataFromModel(request):
	jumlah = Pengguna.objects.all().count()
	if(jumlah > 0):
		pengguna = Pengguna.objects.filter(npm = request.session['kode_identitas'])[0]
		keahlian = Keahlian.objects.filter(pengguna = pengguna)
		response['name'] = pengguna.name
		response['npm'] = pengguna.npm
		response['email'] = pengguna.email
		response['link'] = pengguna.profile
		response['linked'] = True
		response['keahlian'] = keahlian

	else:
		response['name'] = 'kosong'
		response['npm'] = 'kosong'
		response['email'] = 'kosong'
		response['link'] = 'kosong'
def isIntegrated():
	jumlah = Pengguna.objects.all().count()
	return jumlah > 0
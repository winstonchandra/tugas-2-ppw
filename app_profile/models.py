from django.db import models
from django.contrib.postgres.fields import JSONField
# Create your models here.
class Pengguna(models.Model):
	nama = models.TextField()
	npm = models.TextField()
	email = models.TextField()
	profile = models.TextField()

class Keahlian(models.Model):
	jenis = models.TextField()
	level = models.TextField()
	pengguna = models.ForeignKey(Pengguna, on_delete = models.CASCADE)
from django.shortcuts import render
from .api_riwayat import get_matkul
from app_status.models import Status
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
	if 'user_login' in request.session:
		response = {'author':'Kelompok 5C','name':request.session['user_login']}
		response['login'] = True
		response['matkuls'] = get_matkul().json()
		status = Status.objects.all()
		feed = Status.objects.all().count()
		response['feed'] = feed
		if feed > 0:
			response['last'] = Status.objects.last().description
		else:
			response['last'] = 'Oops! Belum ada status'
		html = 'app_riwayat/app_riwayat.html'
		return render(request,html,response)
	else:
		response = {'author':'Kelompok 5C'}
		response['login'] = False
		return HttpResponseRedirect('/login/')
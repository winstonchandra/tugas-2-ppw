from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class RiwayatUnitTest(TestCase):
	def test_riwayat_url_exists(self):
		response = Client().get('/app-riwayat/')
		self.assertEqual(response.status_code, 302)
	def test_riwayat_using_index_func(self):
		found = resolve('/app-riwayat/')
		self.assertEqual(found.func, index)
"""tp2ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic.base import RedirectView
import app_login.urls as app_login
import app_status.urls as app_status
import app_riwayat.urls as app_riwayat
import app_profile.urls as profile
import app_carimahasiswa.urls as app_carimahasiswa

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include(app_login,namespace='login')),
    url(r'^status/', include(app_status,namespace='status')),
    url(r'^app-riwayat/', include(app_riwayat,namespace='app-riwayat')),
    url(r'^profile/', include(profile, namespace = 'profile')),
    url(r'^carimahasiswa/', include(app_carimahasiswa,namespace='carimahasiswa')),
    url(r'^$', RedirectView.as_view(url = "/login/", permanent = "true"), name = 'index'),
]

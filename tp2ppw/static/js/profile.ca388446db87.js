// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
    $.post(
    	'/profile/integrate/',
    	{
    		'first-name': data.first-name,
    		'last-name' : data.last-name,
    		'foto' 		: data.picture-url,
    		'link'		: data.public-profile-url,
    		'email'		: data.email-address
    	},
    	function(response){
    		console.log(response)
    	}
    	)
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~").result(onSuccess).error(onError);
}

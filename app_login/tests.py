from django.test import TestCase
from django.test import Client
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class LoginUnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")

	def test_login_url_is_exist(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_login_failed(self):
		response = self.client.post('/login/custom_auth/login/', {'username': "siapa", 'password': "saya"})
		html_response = self.client.get('/login/').content.decode('utf-8')

	def test_logout(self):
		response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.post('/login/custom_auth/logout/')
		html_response = self.client.get('/login/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)
	
	def test_login_page_when_user_is_logged_in_or_not(self):
		#not logged in, render login template
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('app_login/login.html')

		#logged in, redirect to profile page
		response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/status/')
		self.assertEqual(response.status_code, 302)
		test = 'Anonymous'
		response_post = Client().post('/status/add_status/', {'description': test})
		self.assertEqual(response_post.status_code, 302)
		response = self.client.get('/status/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('app_profile/profile.html')

	# def test_direct_access_to_profile_url(self):
		# #not logged in, redirect to login page
		# response = self.client.get('/profile/')
		# self.assertEqual(response.status_code, 302)

		# #logged in, render profile template
		# response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
		# self.assertEqual(response.status_code, 302)
		# response = self.client.get('/profile/')
		# self.assertEqual(response.status_code, 200)

	#csui_helper.py
	def test_invalid_sso_exception(self):
		username = "winston"
		password = "winston"
		with self.assertRaises(Exception) as context:
			get_access_token(username, password)
		self.assertIn("winston", str(context.exception))
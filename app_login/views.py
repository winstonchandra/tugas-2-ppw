from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.

response = {}

def index(request):
	response['author'] = "Kelompok 5 C"
	if 'user_login' in request.session:
		response['author'] = get_data_user(request, 'user_login')
		return HttpResponseRedirect(reverse('profile:index'))
	else:
		response['author'] = get_data_user(request, 'user_login')
		html = 'app_login/login.html'
		return render(request, html, response)

def get_data_user(request, tipe):
	data = None
	if tipe == "user_login" and 'user_login' in request.session:
		data = request.session['user_login']
	return data

from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Please write your input here',
    }
    
    description_attrs = {
        'type': 'text',
        'cols': 90,
        'rows': 4,
        'class': 'status-form-textarea',
        'placeholder':"Type in your status here...",
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

# class Comment_Form(forms.Form):
    # error_messages = {
        # 'required': 'Tolong isi input ini',
    # }

    # description_attrs ={
        # 'type': 'text',
        # 'cols': 60,
        # 'rows': 10,
        # 'class': 'comment-form-textarea',
        # 'placeholder':' Write your comment here. . '
    # }
    # comment = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

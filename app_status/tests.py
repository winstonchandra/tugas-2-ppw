from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_status, delete_status
from .models import Status
from .forms import Status_Form

# Create your tests here.
class StatusUnitTest(TestCase):

	def test_status_url_is_exist(self):
		response = Client().get('/status/')
		self.assertEqual(response.status_code, 302)

	def test_status_using_index_func(self):
		found = resolve('/status/')
		self.assertEqual(found.func, index)

	def test_model_can_update_status(self):
		# Creating a new status
		new_status = Status.objects.create(description='mantap jiwah')

		# Retrieving all status
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status,1)

	def test_form_validation_for_blank_items(self):
		form = Status_Form(data={'description': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['description'],
			["This field is required."]
		)

	def test_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/status/add_status/', {'description': test})
		self.assertEqual(response_post.status_code, 302)

		# response= Client().get('/status/')
		# html_response = response.content.decode('utf8')
		# self.assertIn(test, html_response)

	def test_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/status/add_status/', {'description': ''})
		self.assertEqual(response_post.status_code, 302)
		
		response= Client().get('/status/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_delete_status(self):
		new_activity = Status.objects.create(description='Update ah')
		#Retrieving all available activity
		object_id = Status.objects.all().count()
		delete_status(Status,object_id)

		after_delete = Status.objects.all().count()
		self.assertEqual(after_delete,0)
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Status
from .forms import Status_Form
from app_profile.models import Pengguna

# Create your views here.
response = {}

def index(request):
    if 'user_login' in request.session:
        response['login'] = True
        response['author'] = "Kelompok 5C"
        status = reversed(Status.objects.all())
        s = Status.objects.all()
        response['status'] = status
        html = 'app_status/status.html'
        response['status_form'] = Status_Form
        response['app'] = "Status"
        response['name'] = request.session['user_login']
        feed = Status.objects.all().count()
        response['feed'] = feed
        if feed>0:
            response['last'] = Status.objects.last().description
        else:
            response['last'] = 'Oops! Belum ada status'
        return render(request, html, response)
    else:
        response['login'] = False
        return HttpResponseRedirect('/login/')

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')

def delete_status(request, id_status):
    status = Status.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/status/')
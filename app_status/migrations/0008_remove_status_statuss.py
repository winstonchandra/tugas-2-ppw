# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-13 06:56
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_status', '0007_status_statuss'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='status',
            name='statuss',
        ),
    ]

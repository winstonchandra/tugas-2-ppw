[![pipeline status](https://gitlab.com/winstonchandra/tugas-2-ppw/badges/master/pipeline.svg)](https://gitlab.com/winstonchandra/tugas-2-ppw/commits/master)

[![coverage report](https://gitlab.com/winstonchandra/tugas-2-ppw/badges/master/coverage.svg)](https://gitlab.com/winstonchandra/tugas-2-ppw/commits/master)

# Tugas 2 PPW

Pengembangan Aplikasi Web dengan TDD, Django, Models, HTML, CSS

## Kelompok 5 Kelas C:
1. Firandra Savitri (1606829043) - Riwayat
2. Nabilah Badriyah Gelshirani - Cari Mahasiswa
3. Tatag Aziz Prawiro - Profile
4. Winston Chandra (1606831294) - Login dan Status

## Link HerokuApp:

https://tugas2ppwk5c.herokuapp.com/
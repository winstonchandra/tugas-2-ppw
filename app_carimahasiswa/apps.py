from django.apps import AppConfig


class AppCarimahasiswaConfig(AppConfig):
    name = 'app_carimahasiswa'

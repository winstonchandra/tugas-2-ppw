from django.shortcuts import render
from django.http import HttpResponseRedirect
from app_status.models import Status

# Create your views here.
response = {}
def index(request):    
	if 'user_login' in request.session:
		response['login'] = True
		response['author'] = "Kelompok 5C"
		s = Status.objects.all()
		response['name'] = request.session['user_login']
		feed = Status.objects.all().count()
		response['feed'] = feed
		if feed>0:
			response['last'] = Status.objects.last().description
		else:
			response['last'] = 'Oops! Belum ada status'
		html = 'app_carimahasiswa/app_carimahasiswa.html'
		return render(request, html, response)
	else:
		response['login'] = False
		return HttpResponseRedirect('/login/')